/**
 * Created by lenovo on 2016/10/8.
 */
angular.module("myApp",[])
    .controller("cartController",function($scope){
        $scope.cart=[
            {
                id:1000,
                name:"iphone5s",
                quantity:3,
                price:4300
            },
            {
                id:3300,
                name:"iphone5",
                quantity:30,
                price:3300
            },
            {
                id:232,
                name:"imac",
                quantity:3,
                price:23000
            },
            {
                id:1002,
                name:"ipad",
                quantity:5,
                price:6900
            }
        ];
        //计算总购买价
        $scope.totalPrice=function(){
            var total=0;
            angular.forEach($scope.cart,function(item){
                total+=item.quantity*item.price;
            });
            return total;
        };
    //    计算总购买数量
        $scope.totalCount=function(){
            var total=0;
            angular.forEach($scope.cart,function(item){
                total+=parseInt(item.quantity);
            });
            return total;
        };
    //    找一个元素的索引
    //    添加某一项的数量

    //    移除操作
        $scope.remove=function(id){
            var index=-1;
            angular.forEach($scope.cart,function(item,key){
                if(item.id===id){
                    index=key;
                }
            });
            if(index!==-1){
                $scope.cart.splice(index,1);
            }
        }
    });
