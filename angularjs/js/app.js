/**
 * Created by lenovo on 2016/9/23.
 */
    angular.module("exerciseApp",['ngRoute','ngAnimate','ExerciseCtrls'])
        .config(['$routeProvider',function($routeProvider){
            $routeProvider.when('/hello',{
                templateUrl:'hello.html',
                controller:'HelloCtrl'
            });
            $routeProvider.when('/list',{
                templateUrl:'booklist.html',
                controller:'ListCtrl'
            });
            $routeProvider.when('/form',{
                templateUrl:'form.html',
                controller:'FormCtrl'
            });
            $routeProvider.when('/css',{
                templateUrl:'css-style.html',
                controller:'CssCtrl'
            });
            $routeProvider.when('/show',{
                templateUrl:'show-hide.html',
                controller:'ShowCtrl'
            });
            $routeProvider.otherwise({redirectTo: '/hello'});
        }])
        .directive("hello",function(){
            return{
                restrict:"ACEM",
                template:'<div>hi everyone!</div>',
                replace:true
            }
        });
